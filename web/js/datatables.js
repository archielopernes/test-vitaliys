$(document).ready(function() {
	if($('#transactions-table').length > 0) {
		$('#transactions-table').DataTable({
			"columnDefs": [
				{
	                "targets": [ 0 ],
	                "visible": false,
	                "searchable": false
            	},
			],
			"order" : [[ 0, "desc" ]]		});
	}
});

