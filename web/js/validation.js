$(document).ready(function() {
	if($('#registerContainer').length > 0) {
		var mode = $('#registerContainer').data('mode');
		var type = '';
		var msg = '';
		if(mode == 1) {
			type= 'success';
			msg = 'Account successfully saved. <a href="/">Click to login</a>';
		} else if (mode == 2) {
			type = 'error';
			msg = 'An error while saving data';
		}

		if(mode != 0) {
			swal({
			  title: "",
			  text: msg,
			  type: type,
			  html: true
			});
		}
	} else if ($('#transferContainer').length > 0) {
        var mode = $('#transferContainer').data('mode');
		if(mode == 2 || mode == 3) {
            var msg = 'An error while saving data';
            if (mode == 3) {
                msg = 'You are using your nickname';
            }
            swal({
			  title: "",
			  text: msg,
			  type: 'error',
			  html: true
			});
		}
    }
});