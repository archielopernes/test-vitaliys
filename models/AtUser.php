<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\Model;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\helpers\Url;


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $nickname
 *
 * @property Money[] $moneys
 */
class AtUser extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nickname'], 'required'],
            [['nickname'], 'unique'],
            [['nickname'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nickname' => 'Nickname',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneys()
    {
        return $this->hasMany(AtMoney::className(), ['user_id' => 'id']);
    }

    /******************************** Identity Interface ********************************/
    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

     /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /******************************** Custom Functions ********************************/

    public static function findByNickname($nickname)
    {
        return static::findOne(['nickname' => $nickname]);
    }

    public function findRecords($condition=null)
    {
        if(empty($condition)) {
            return self::find()->all();
        } else {
            return self::find()->where($condition)->all();
        }
        
    }

    public function saveAccount($model, $values) 
    {
        $connection = Yii::$app->db;
        $transaction =  $connection->beginTransaction();
        try {
            $model->nickname = strtolower($values['nickname']);
            if($model->save()) {
                $moneyModel = new AtMoney; 
                $moneyModel->user_id = $model->id; 
                $moneyModel->transaction_type = 'initial'; 
                $moneyModel->amount = $moneyModel->current_balance = 0;
                if($moneyModel->save()) {
                    $transaction->commit();
                    return $model;
                } else {
                    var_dump($moneyModel->errors);
                    $transaction->rollBack();
                    return false;
                }
            } else {
                var_dump($model->errors);
                $transaction->rollBack();
                return false;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }

    }
}
