<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\helpers\Url;

/**
 * This is the model class for table "money".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $transaction_type
 * @property integer $to_user_id
 * @property integer $from_user_id
 * @property double $amount
 * @property double $current_balance
 *
 * @property User $user
 */
class AtMoney extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'transaction_type', 'amount', 'current_balance'], 'required'],
            [['user_id', 'to_user_id', 'from_user_id'], 'integer'],
            [['transaction_type'], 'string'],
            [['amount', 'current_balance'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => AtUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'transaction_type' => 'Transaction Type',
            'to_user_id' => 'To User ID',
            'from_user_id' => 'From User ID',
            'amount' => 'Amount',
            'current_balance' => 'Current Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(AtUser::className(), ['id' => 'user_id']);
    }

    public function getLastBalance($user_id)
    {
        return self::find()->where(['user_id' => $user_id])->orderBy('id DESC')->one();
    }
    
    public function saveTransfer($postedValues, $user_id, $to_user_id)
    {
        $connection = Yii::$app->db;
        $transaction =  $connection->beginTransaction();
        
        try {
            $userInfo = $this->getLastBalance($user_id);
            $toUserInfo = $this->getLastBalance($to_user_id);
            /* Transfer Money */
            $model = new AtMoney; 
            $model->user_id = $user_id;
            $model->transaction_type = 'transfer';
            $model->to_user_id = $to_user_id;
            $model->amount = floatval($postedValues['amount']);
            $model->current_balance = floatval($userInfo->current_balance) + ($model->amount * -1);
            if($model->save()) {
                $model2 =  new AtMoney;
                $model2->user_id = $to_user_id;
                $model2->transaction_type = 'receive';
                $model2->from_user_id = $user_id;
                $model2->amount = floatval($postedValues['amount']);
                $model2->current_balance = $toUserInfo->current_balance + $model->amount;
                if($model2->save()) {
                    $transaction->commit();
                    return true;
                } else {
                    var_dump($model2->errors);
                    $transaction->rollBack();
                    return false;
                }
            } else {
                var_dump($model->errors);
                $transaction->rollBack();
                return false;
            }
            
            
        } catch (Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }
    
    public function getTransactions($userId)
    {
        $query = new Query;
		$query->params([':userid' => $userId]);
        $query->select('*')
              ->from('money')
              ->where('user_id=:userid AND transaction_type IN ("transfer", "receive")')
              ->orderBy('id DESC');
        $command = $query->createCommand(Yii::$app->db);
        $rows = $command->queryAll();
        return $rows;
    }

    public function getAllBalances()
    {
        $query = new Query;
        $query->select('*')
              ->from('money')
              ->where('id in ( select MAX(id) from money group by user_id )');
        $command = $query->createCommand(Yii::$app->db);
        $rows = $command->queryAll();
        return $rows;
    }
}
