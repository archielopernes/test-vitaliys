<?php

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => true,
    'rules' => [
        /* Automatic referencing for urls <module>/controller/action format */
        ''
        => '/account/session/login',
        '/<action:(login|logout|dashboard|register)>'
        => '/account/session/<action>',


        '/<module:transaction>/<controller:manage>/transfer'
        => '/<module>/<controller>/index',
        '/<module:transaction>/<controller:manage>/<action:list>'
        => '/<module>/<controller>/<action>'
    ]
];
?>
