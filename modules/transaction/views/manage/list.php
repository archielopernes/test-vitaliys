<?php
use app\assets\DatatablesAsset;
DatatablesAsset::register($this);
$this->title = 'Summary of Transactions';
setlocale(LC_MONETARY, 'en_US');
?>

<h2 class="title"><?php echo $this->title; ?></h2><span class="line"></span>
<div class="content">
    <table id="transactions-table" class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Account Nickname</th>
                <th>Account Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($records as $rec) { 
                if($rec['transaction_type'] == 'receive') {
                    $receiver = $rec['user_id'];
                    $sender = $rec['from_user_id'];
                } else {
                    $receiver = $rec['to_user_id'];
                    $sender = $rec['user_id'];
                }
            ?>
                <tr>
                    <td><?= $rec['id'] ?></td>
                    <td><?= (array_key_exists($rec['user_id'], $users)) ? ucfirst($users[$rec['user_id']]) : ''; ?></td>
                    <td><?= money_format('%.2n', $rec['current_balance']); ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
