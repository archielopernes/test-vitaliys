<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AlertAsset;

AlertAsset::register($this);
$this->title = 'Money Transfer';
?>

<h2 class="title"><?php echo $this->title; ?></h2><span class="line"></span>
<div class="content" id="transferContainer" data-mode = "<?= $mode ?>">
    <div class="container">
	<?php $form = ActiveForm::begin([
                    'id' 		=> 'transfer-form',
                    'method'    => 'post',
                    'layout'	=> 'horizontal'
                    ]);
    ?>
        <div class="row center-block" style="width:60%;">
            <!-- Account Nickname -->
            <div class="col-md-3 text-right"><label>Nickname</label></div>
            <div class="col-md-9">
                <input type="text" class="form-control toLower" name="AtMoney[nickname]" required>
            </div>
            <div class="clearfix"></div><br>
            <!-- Amount -->
            <div class="col-md-3 text-right"><label>Amount</label></div>
            <div class="col-md-9">
                <input type="number" step="0.01" min="0.01" class="form-control" name="AtMoney[amount]" required>
            </div>
            <div class="clearfix"></div><br>
            <div class="col-md-12 text-right">
            	<button type="submit" class="btn btn-primary" id="transfer-btn">Transfer Money</button>	
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>