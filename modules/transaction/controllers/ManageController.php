<?php

namespace app\modules\transaction\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\AtUser;
use app\models\AtMoney;
use app\components\helpers\Data;

class ManageController extends \yii\web\Controller
{
	public $layout = '/commonLayout';
    public $route_nav;
    public $viewPath = 'app/modules/transaction/views';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Pages that are included in the rule set
                'only'  => ['index', 'list'],
                'rules' => [
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['index'],
                        'roles'     => ['@']
                    ],
                     [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['list'],
                        'roles'     => ['?']
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    // Action when user is denied from accessing a page
                    if (Yii::$app->user->isGuest) {
                        $this->goHome();
                    } else {
                        $this->redirect(['/dashboard']);
                    }
                }
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init()
    {
        if(Yii::$app->user->isGuest) {
            $url = '/login';
        } else {
            $url = '/dashboard';
        }
    }

    public function actionIndex()
    {
    	$mode = 0;
    	$model = new AtMoney;
    	$userModel = new AtUser;
    	$identity = Yii::$app->user->identity; 
    	if(Yii::$app->request->isPost) {
    		$postedValues = Yii::$app->request->post('AtMoney');
            if($identity->nickname != strtolower($postedValues['nickname'])) {
                $record = $userModel->findRecords(['nickname' => strtolower($postedValues['nickname'])]);
                if(empty($record)) {
                    if(!$record = $userModel->saveAccount($userModel, ['nickname' => $postedValues['nickname']])) {
                        $saveTransfer = true; 
                    } else {
                        $mode = 2; 
                    }
                } else {
                    $record = array_shift($record);
                    $saveTransfer = true; 
                }

                if(isset($saveTransfer) && $saveTransfer) {
                    if($model->saveTransfer($postedValues, $identity->id, $record->id)) {
                        return $this->redirect('/dashboard');
                    }
                } else {
                    $mode = 2;
                }
            } else {
                $mode = 3; // Nickname is the same    
            }
    	}

    	return $this->render('transaction', [
    		'mode'		=> $mode	
    	]);
    }

    /* Public List */
    public function actionList()
    {
        $model = new AtUser;
        $moneyModel = new AtMoney;
        $users = $model->findRecords();
        $arr = [];
        foreach($users as $user) {
            $arr[$user->id] = $user->nickname;
        }
        $records = $moneyModel->getAllBalances();
        return $this->render('list', [
                            'users'     => $arr,
                            'records'   => $records
        ]);
    }
  
}

?>
