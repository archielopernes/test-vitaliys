<?php

namespace app\modules\transaction;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\transaction\controllers';
    public $defaultRoute = '';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

?>
