<?php

namespace app\modules\account\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\AtUser;
use app\models\AtMoney;
use app\models\LoginForm;

class SessionController extends \yii\web\Controller
{
    public $layout = '/loginLayout';
    public $route_nav;
    public $viewPath = 'app/modules/account/views';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Pages that are included in the rule set
                'only'  => ['index', 'login', 'logout', 'register'],
                'rules' => [
                    [ // Pages that can be accessed without logging in
                        'allow'     => true,
                        'actions'   => ['login', 'register'],
                        'roles'     => ['?']
                    ],
                    [ // Pages that can be accessed when logged in
                        'allow'     => true,
                        'actions'   => ['logout'],
                        'roles'     => ['@']
                    ]
                ],
                'denyCallback' => function ($rule, $action) {
                    // Action when user is denied from accessing a page
                    if (Yii::$app->user->isGuest) {
                        $this->goHome();
                    } else {
                        $this->redirect(['/dashboard']);
                    }
                }
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init()
    {
        if(Yii::$app->user->isGuest) {
            $url = '/login';
        } else {
            $url = '/dashboard';
        }
    }

    public function actionLogin()
    {
        $model = new LoginForm;

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = Yii::$app->user->getIdentity()->attributes;
            $session = Yii::$app->session;
            $session->open();
            foreach ($user as $userKey => $userValue) {
                $session[$userKey] = $userValue;
            }
            $session->close();

            $this->redirect(['/dashboard']);
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->removeAll();          // Removes all the session variables
        $session->destroy();            // Destroy session
        Yii::$app->response->clear();   // Clears the headers, cookies, content, status code of the response.
        Yii::$app->user->logout();
        $this->goHome();
    }

    public function actionDashboard()
    {
        $this->layout = '/commonLayout';
        $model = new AtUser;
        $moneyModel = new AtMoney;
        $users = $model->findRecords();
        $arr = [];
        foreach($users as $user) {
            $arr[$user->id] = $user->nickname;
        }
        $records = $moneyModel->getTransactions(Yii::$app->user->identity->id);
        return $this->render('dashboard', [
                            'users'     => $arr,
                            'records'   => $records
        ]);
    }

    public function actionRegister()
    {
        $this->layout = '/commonLayout';
        $model = new AtUser;
        $mode = 0; // Registration
        if(Yii::$app->request->isPost) {
            $postedData = Yii::$app->request->post('AtUser');
            $return = $model->saveAccount($model, $postedData);
            if(!empty($return)) {
                $mode = 1; // Successfully registered
            } else {
                $mode = 2; // Error 
            }
        } 

        return $this->render('register', 
        [
            'model'     => $model,
            'mode'      => $mode
        ]);
    }

}

?>
