<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AlertAsset;

AlertAsset::register($this);
$this->title = 'Register Account';
?>

<h2 class="title"><?php echo $this->title; ?></h2><span class="line"></span>
<div class="content" id="registerContainer" data-mode = "<?= $mode ?>">
	<?php $form = ActiveForm::begin([
                    'id' 		=> 'register-form',
                    'method'    => 'post',
                    'layout'	=> 'horizontal'
                    ]);
    ?>

    <?= $form->field($model, 'nickname')->textInput(array('class'=>'form-control toLower')) ?>
    <div class="col-md-12 text-center">
    	<button type="submit" class="btn btn-primary" id="register-btn">Register Account</button>	

    </div>

    <?php ActiveForm::end(); ?>
</div>