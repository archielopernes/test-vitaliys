<?php
use app\assets\DatatablesAsset;
DatatablesAsset::register($this);
$this->title = 'Summary of Transactions';
setlocale(LC_MONETARY, 'en_US');
?>

<h2 class="title"><?php echo $this->title; ?></h2><span class="line"></span>
<div class="content">
    <table id="transactions-table" class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Transaction Type</th>
                <th>Sender / Receiver</th>
                <th>Amount</th>
                <th>Account Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($records as $rec) { 
                $type = ($rec['transaction_type'] == 'receive') ? 'from_user_id' : 'to_user_id'; 
                $type = $rec[$type];
                $user = '';
                if(array_key_exists($type, $users)) {
                    $user = $users[$type];
                } 
            ?>
                <tr>
                    <td><?= $rec['id'] ?></td>
                    <td><?= ucfirst($rec['transaction_type']) ?></td>
                    <td><?= $user ?></td>
                    <td><?= money_format('%.2n', $rec['amount']); ?></td>
                    <td><?= money_format('%.2n', $rec['current_balance']); ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
