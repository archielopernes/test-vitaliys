<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    $this->title = 'Login';
?>

<div id="login-wrapper">
    <div id="logo">
        <img src='/resources/common/logo.png'>
    </div>
    <div id="content">
        <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'method'    => 'post',
                    'fieldConfig' => [
                            'template' => "<div class=\"control-group\">{input}</div>\n<div>{error}</div>"
                    ]]);
        ?>

        <?= $form->field($model, 'nickname')->textInput(array('placeholder'=>'Nickname')) ?>

        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary pull-right', 'name' => 'login-button']) ?>
        <span id="registerText">Not yet registered? <a href="/register">Sign up here</a></span>
        <br>
        <span><a href="/transaction/manage/list">See all transactions</a></span>
        <?php ActiveForm::end(); ?>
    </div>
</div>
